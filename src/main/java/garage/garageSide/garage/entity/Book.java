package garage.garageSide.garage.entity;

import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
//import java.util.Date;
import java.util.*;
import java.time.LocalDate; // import the LocalDate class
import java.time.LocalTime; // import the LocalDate class
import java.text.SimpleDateFormat;
import java.text.DateFormat;

@Entity
@Table(name = "bookeddetails")
public class Book {

//    DateFormat f = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
//    Date d = f.parse("8/29/2011 11:16:12 AM");
//    DateFormat date = new SimpleDateFormat("MM/dd/yyyy");
//    DateFormat time = new SimpleDateFormat("hh:mm:ss a");
//        System.out.println("Date: " + date.format(d));
//        System.out.println("Time: " + time.format(d));

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @UniqueElements
    @Column(name = "numberplate")
    private String numberPlate;

    @Column(name = "date")
    private LocalDate localDate;
//
//    @Column(name = "duration")
//    private int duration;

    @Column(name = "time")
    private String localTime;

    @Column(name = "terminalno")
    private long terminalNo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public long getTerminalNo() {
        return terminalNo;
    }

    public void setTerminalNo(long terminalNo) {
        this.terminalNo = terminalNo;
    }

    public Book(String name, @UniqueElements String numberPlate, LocalDate localDate, int duration, String localTime, long terminalNo) {
        this.name = name;
        this.numberPlate = numberPlate;
        this.localDate = localDate;
        this.duration = duration;
        this.localTime = localTime;
        this.terminalNo = terminalNo;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", numberPlate='" + numberPlate + '\'' +
                ", localDate=" + localDate +
                ", duration=" + duration +
                ", localTime=" + localTime +
                ", terminalNo=" + terminalNo +
                '}';
    }
}
